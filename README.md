# Talana Feed App
Prueba de conocimientos.

### Pre-requisitos 📋
Instalar previamente CocoaPods y SwiftLint

### Descripción y funcionalidades 🔧

Arquitectura Implementada: 📌
He decidido implementar VIPER para la prueba, porque aunque aunque la app solo consta de 3 pantallas, considero que para IOS es la que mejor posibilidad de aplicar SOLID en un proyecto, 
ademas de permitir hacer Unit test de Una manera mas Clara y Limpia

Patrones de diseño: 📌
📦 Builder: He optado por usar el patron creacional Builder para la construccion de la Capa de Red, ya que brinda la posibilidad de que aplicaciones que seran migradas a SwiftUI en un futuro, puedan continuar siendo desarrolladas a partir de lo que es la programacion funcional.

📦 Injeccion de Dependecias:
He usado la Inyeccion de Dependencias por medio de los Properties Wrappers, el cual brinda la posibilidad de evitar la inyeccion de dependencias por constructor, que por mas que se inyecten protocolos para evitar el acoplamiento, conlleva a estar pasando una seria de parametros en sus constructures.
otra ventaja es que el tema de los Unit Test con este enfoque es bastante efectivo, ya que en determinado momento dedidimos si se inyecta un mock o si se inyecta una funcionalidad de produccion.

Herramientas de Calidad de Codigo: 📌
He usado Swiftlint como herramienta de calidad de codigo.

- Librerias Usadas: 📌
He usado las siguientes Librerias y el porque de su uso y de sus ventajas.
📦 IGListKit -> Libreria que permite trabajar con lista, siendo capaz de detectar canbios en el datasource, y refrescando la vista, animaciones, y cache de los datos, que permites un performance de la app.

📦 Nuke -> es una libreria muy buena para cargar imaganes desde intertet, que ademas de mantenerla en cache, permite configuracion como: poner imagen como placeholder miestras la imagen se carga, imagenes de error, por que la descarga de la imagen falla, entre otras bondades.

- Otras Consideraciones:
- Se tiene una clase "constraintHelper+UIView" esta clase permite que apartir de crear un componente desde codigo, podamos setear sus constraint de manera mas amigable, que como es sabido es la mejor manera para brindar un mejor performance en la carga de vistas.

- LoginUI, HomeUI, DetailUI -> estos archivos brindan la posibilidad a travez de objetos "Object" desde el IB, la manera de desacoplar desde los viewcontroller, todo lo que es la customizacion de la vista y delegar esta responsabilidad a una clase externa.


## Pendiente pos implementar ⚙️

XibLint -> Reglas de lint para diseño
Periferi -> Herramienta para monitorear codigo en desuso.
Services para el AppDelegate -> Forma de optimizar el App Delegate y evitar que en un mismo metodo de inicializacion, se mesclen multiple logica de distintas librerias o SDK's
