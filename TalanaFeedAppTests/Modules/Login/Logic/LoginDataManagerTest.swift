//
//  LoginDataManagerTest.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import XCTest
@testable import TalanaFeedApp

class LoginDataManagerTest: XCTestCase {
    @Injected(\.networkManager) var networkManager: NetworkManagerProtocol
    var sut: LoginDataManager?

    override func setUp() {
     sut = LoginDataManager()
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testGetLoginSuccess() {
        InjectedValues[\.networkManager] = NetworkMock(isSuccessResult: true)
        let request = LoginRequest(username: "user", password: "pswd")
        sut?.getLogin(request: request) { response in
            switch response {
            case .success(let login):
                XCTAssertNotNil(login)
            case .failure:
                XCTFail("fail - login has been success")
            }
        }
    }

    func testGetLoginFail() {
        InjectedValues[\.networkManager] = NetworkMock(isSuccessResult: false)
        let request = LoginRequest(username: "", password: "")
        sut?.getLogin(request: request) { response in
            switch response {
            case .success:
                XCTFail("fail - login has been success")
            case .failure(let error):
                XCTAssertTrue(error.statusCode == 400)
                XCTAssertNotNil(error)
            }
        }
    }
}
