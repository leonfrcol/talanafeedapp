//
//  LoginInteractorTest.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import XCTest
@testable import TalanaFeedApp

class LoginInteractorTest: XCTestCase {
    @Injected(\.loginDataManager) var loginDataManager: LoginDataManagerProtocol
    var sut: LoginInteractor?
    var presenter: LoginPresenterMock?

    override func setUp() {
        sut = LoginInteractor()
        presenter = LoginPresenterMock()
        sut?.presenter = presenter
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func testFetchDataLoginSuccess() {
        InjectedValues[\.loginDataManager] = LoginDataManagerMock(isSuccessResult: true)
        sut?.login(request: LoginRequest(username: "user", password: "pwd"))
        XCTAssertTrue(presenter?.isSuccess == true)
        XCTAssertNil(presenter?.error)
    }

    func testFetchDataLoginFail() {
        InjectedValues[\.loginDataManager] = LoginDataManagerMock(isSuccessResult: false)
        sut?.login(request: LoginRequest(username: "user", password: "pwd"))
        XCTAssertTrue(presenter?.isSuccess == false)
        XCTAssertNotNil(presenter?.error)
    }
}
