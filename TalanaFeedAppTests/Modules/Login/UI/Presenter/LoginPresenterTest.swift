//
//  LoginPresenterTest.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import XCTest
@testable import TalanaFeedApp

class LoginPresenterTest: XCTestCase {
    var sut: LoginPresenter?
    var mockInteractor: LoginInteractorMock?
    var mockRouter: LoginRouterMock?

    override func setUp() {
        sut = LoginPresenter()
        mockInteractor = LoginInteractorMock()
        mockRouter = LoginRouterMock()

        mockInteractor?.presenter = sut
        sut?.interactor = mockInteractor
        sut?.router = mockRouter
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        mockInteractor = nil
        mockRouter = nil
        super.tearDown()
    }

    func testLoginCallSuccess() {
        sut?.login(user: "user", password: "pasword")
        XCTAssertTrue(mockInteractor?.isLogindCalled == true, "login has not been called")
    }

    func testLoginCallUserFail() {
        sut?.login(user: nil, password: "pasword")
        XCTAssertTrue(mockInteractor?.isLogindCalled == false, "login has not been called")
    }

    func testLoginCallPasswordFail() {
        sut?.login(user: "user", password: nil)
        XCTAssertTrue(mockInteractor?.isLogindCalled == false, "login has not been called")
    }

    func testGoHome() {
        let asdasd = LoginViewController()
        sut?.goToHome(navController: asdasd.navigationController)
        XCTAssertTrue(mockRouter?.isPresentHomeCalled == true, "login has not been called")
    }

    func testlogginResponseSuccess() {
        let response = LoginResponse(status: "OK", token: "a88cfc8ea5ca5a47f27a5320c23db568")
        mockInteractor?.isFetchLoginSuccessCalled = true
        sut?.logginSuccess(response: response)
        XCTAssertTrue(mockInteractor?.isFetchLoginSuccessCalled == true, "login failed")
    }

    func testlogginResponseFail() {
        mockInteractor?.isFetchLoginErrorCalled = true
        sut?.logginError(error: "Error")
        XCTAssertTrue(mockInteractor?.isFetchLoginErrorCalled == true, "login success")
    }
}
