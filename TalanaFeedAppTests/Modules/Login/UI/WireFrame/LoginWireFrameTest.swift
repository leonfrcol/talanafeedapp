//
//  LoginWireFrameTest.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import XCTest
@testable import TalanaFeedApp

class LoginWireFrameTest: XCTestCase {
    var sut: LoginRouter?
    var mockViewController: HomeViewControllerMock?

    override func setUp() {
        sut = LoginRouter()
        mockViewController = HomeViewControllerMock()
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        mockViewController = nil
        super.tearDown()
    }

    func testHomeShowSuccess() {
        sut?.presentHome(navController: mockViewController?.navigationController)
        mockViewController?.isFeedSuccessCalled = true
        XCTAssertTrue(mockViewController?.isFeedSuccessCalled == true, "home has not been called")
    }

    func testHomeShowFail() {
        sut?.presentHome(navController: mockViewController?.navigationController)
        mockViewController?.isFeedErrorCalled = true
        XCTAssertTrue(mockViewController?.isFeedErrorCalled == true, "home has been called")
    }
}
