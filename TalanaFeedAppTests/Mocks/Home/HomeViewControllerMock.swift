//
//  HomeViewControllerMock.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

@testable import TalanaFeedApp
import UIKit

class HomeViewControllerMock: UIViewController, HomePresenterToViewProtocol {
    var isFeedSuccessCalled = false
    var isFeedErrorCalled = false

    func feedSuccess(listFeed: [FeedModel]) {
        isFeedSuccessCalled = true
    }

    func feedError() {
        isFeedErrorCalled = true
    }
}
