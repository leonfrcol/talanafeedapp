//
//  LoginRouterMock.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import XCTest
@testable import TalanaFeedApp

class LoginRouterMock: LoginPresenterToRouterProtocol {
    var isPresentHomeCalled = false

    static func createModule() -> LoginViewController {
        LoginViewController()
    }

    func presentHome(navController: UINavigationController?) {
        isPresentHomeCalled = true
    }
}
