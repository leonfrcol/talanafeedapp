//
//  LoginPresenterMock.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

@testable import TalanaFeedApp

public class LoginPresenterMock: LoginInteractorToPresenterProtocol {
    public var isSuccess = false
    public var error: String?

    public func logginSuccess(response: LoginResponse) {
        isSuccess = true
    }

    public func logginError(error: String) {
        self.error = error
    }

    public func getFeedSuccess(list: [Feed]) {
        isSuccess = true
    }
}
