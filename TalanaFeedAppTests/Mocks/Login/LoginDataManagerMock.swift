//
//  LoginDataManagerMock.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import Foundation
@testable import TalanaFeedApp

public class LoginDataManagerMock: LoginDataManagerProtocol {
    var isSuccessResult = true

    public init(isSuccessResult: Bool) {
        self.isSuccessResult = isSuccessResult
    }

    public func getLogin(request: LoginRequest, completion: @escaping (Result<LoginResponse, NetworkError>) -> Void) {
        if isSuccessResult {
            let response = LoginResponse(status: "OK", token: "a88cfc8ea5ca5a47f27a5320c23db568")
            completion(.success(response))
        } else {
            let error = NetworkError(statusCode: -1, message: "FAIL")
            completion(.failure(error))
        }
    }
}
