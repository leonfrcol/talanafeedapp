//
//  LoginInteractorMock.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//
@testable import TalanaFeedApp

class LoginInteractorMock: LoginPresenterToInteractorProtocol {
    var presenter: LoginInteractorToPresenterProtocol?
    var isLogindCalled = false
    var isFetchLoginSuccessCalled = false
    var isFetchLoginErrorCalled = false

    func login(request: LoginRequest) {
        isLogindCalled = true
    }
}
