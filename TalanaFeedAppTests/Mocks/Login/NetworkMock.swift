//
//  NetworkMock.swift
//  TalanaFeedAppTests
//
//  Created by DIF on 5/7/22.
//

import UIKit
@testable import TalanaFeedApp

public class NetworkMock: NetworkManagerProtocol {
    var isSuccessResult = true

    public init(isSuccessResult: Bool) {
        self.isSuccessResult = isSuccessResult
    }

    public func execute(with request: URLRequest?, completion: @escaping (Result<Data, NetworkError>) -> Void) {
        if self.isSuccessResult {
            let response = LoginResponse(status: "", token: "")
            if let encoded = try? JSONEncoder().encode(response) {
                completion(.success(encoded))
            }
        } else {
            completion(.failure(NetworkError(statusCode: 400, message: "bad request")))
        }
    }
}
