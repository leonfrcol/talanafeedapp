//
//  DependencyContainerKeys.swift
//  FeedApp
//
//  Created by DIF on 1/7/22.
//

public struct ConfigurationManagerProviderKey: InjectionKey {
    public static var currentValue: ConfigurationProtocol = Configuration()
}

public struct NetworkProviderKey: InjectionKey {
    public static var currentValue: NetworkManagerProtocol = NetworkManager()
}

public struct LoginDataManagerProviderKey: InjectionKey {
    public static var currentValue: LoginDataManagerProtocol = LoginDataManager()
}

public struct HomeDataManagerProviderKey: InjectionKey {
    public static var currentValue: HomeDataManagerProtocol = HomeDataManager()
}

public struct DetailDataManagerProviderKey: InjectionKey {
    public static var currentValue: DetailDataManagerProtocol = DetailDataManager()
}
