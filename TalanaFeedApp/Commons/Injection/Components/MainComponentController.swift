//
//  MainComponentController.swift
//  FeedApp
//
//  Created by DIF on 1/7/22.
//

class MainComponentController {
    func registerAll() {
        InjectedValues()
    }
}

public extension InjectedValues {
    var configurationManager: ConfigurationProtocol {
        get { Self[ConfigurationManagerProviderKey.self] }
        set { Self[ConfigurationManagerProviderKey.self] = newValue }
    }

    var networkManager: NetworkManagerProtocol {
        get { Self[NetworkProviderKey.self] }
        set { Self[NetworkProviderKey.self] = newValue }
    }

    var loginDataManager: LoginDataManagerProtocol {
        get { Self[LoginDataManagerProviderKey.self] }
        set { Self[LoginDataManagerProviderKey.self] = newValue }
    }

    var homeDataManager: HomeDataManagerProtocol {
        get { Self[HomeDataManagerProviderKey.self] }
        set { Self[HomeDataManagerProviderKey.self] = newValue }
    }

    var detailDataManager: DetailDataManagerProtocol {
        get { Self[DetailDataManagerProviderKey.self] }
        set { Self[DetailDataManagerProviderKey.self] = newValue }
    }
}
