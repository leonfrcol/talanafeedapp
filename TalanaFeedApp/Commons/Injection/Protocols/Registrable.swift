//
//  Registrable.swift
//  FeedApp
//
//  Created by DIF on 1/7/22.
//

public protocol Registrable {
    func register()
}
