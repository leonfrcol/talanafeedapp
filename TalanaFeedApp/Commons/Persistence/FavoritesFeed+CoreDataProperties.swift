//
//  FavoritesFeed+CoreDataProperties.swift
//  
//
//  Created by DIF on 5/7/22.
//
//

import UIKit
import Foundation
import CoreData

extension FavoritesFeed {
    @nonobjc
    public class func fetchRequest() -> NSFetchRequest<FavoritesFeed> {
        NSFetchRequest<FavoritesFeed>(entityName: "FavoritesFeed")
    }

    @NSManaged public var id: Int16
    @NSManaged public var title: String?
    @NSManaged public var image: String?
    @NSManaged public var date: String?
    @NSManaged public var attribute: String?
    @NSManaged public var published: String?
    @NSManaged public var author_id: String?
}
