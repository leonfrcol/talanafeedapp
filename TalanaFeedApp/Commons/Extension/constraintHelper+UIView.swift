//
//  constraintHelper+UIView.swift
//  TalanaFeedApp
//
//  Created by DIF on 4/7/22.
//

import UIKit

public extension UIView {
    func addSubviewForAutoLayout(_ subView: UIView? = nil) {
        guard let sView = subView else {
            return
        }
        sView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(sView)
    }

    func anchor (
        top: NSLayoutYAxisAnchor? = nil,
        left: NSLayoutXAxisAnchor? = nil,
        bottom: NSLayoutYAxisAnchor? = nil,
        right: NSLayoutXAxisAnchor? = nil,
        leading: NSLayoutXAxisAnchor? = nil,
        trailing: NSLayoutXAxisAnchor? = nil,
        paddingTop: CGFloat = 0,
        paddingLeft: CGFloat = 0,
        paddingBottom: CGFloat = 0,
        paddingRight: CGFloat = 0,
        width: CGFloat? = nil,
        height: CGFloat? = nil
    ) {
        translatesAutoresizingMaskIntoConstraints = false

        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }

        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }

        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }

        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }

        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }

        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }

        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading).isActive = true
        }

        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing).isActive = true
        }

        layoutIfNeeded()
    }

    func center(
        inView view: UIView,
        yConstant: CGFloat? = 0
    ) {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: yConstant ?? 0).isActive = true
    }

    func centerX(
        inView view: UIView,
        topAnchor: NSLayoutYAxisAnchor? = nil,
        paddingTop: CGFloat? = 0
    ) {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        if let topAnchor = topAnchor {
            self.topAnchor.constraint(equalTo: topAnchor, constant: paddingTop ?? 0).isActive = true
        }
    }

    func centerY(
        inView view: UIView,
        leftAnchor: NSLayoutXAxisAnchor? = nil,
        paddingLeft: CGFloat? = nil,
        constant: CGFloat? = 0
    ) {
        translatesAutoresizingMaskIntoConstraints = false

        centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: constant ?? 0).isActive = true

        if let leftAnchor = leftAnchor, let padding = paddingLeft {
            self.leftAnchor.constraint(equalTo: leftAnchor, constant: padding).isActive = true
        }
    }
}
