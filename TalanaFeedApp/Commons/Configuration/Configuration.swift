//
//  Configuration.swift
//  TalanaFeedApp
//
//  Created by DIF on 3/7/22.
//

import UIKit

public protocol ConfigurationProtocol {
    var environment: EnvironmentProtocol { get }
}

public struct Configuration: ConfigurationProtocol {
    public var environment: EnvironmentProtocol = {
        Environment.production
    }()
}
