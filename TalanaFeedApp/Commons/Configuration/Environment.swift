//
//  Environment.swift
//  FeedApp
//
//  Created by DIF on 1/7/22.
//

public protocol EnvironmentProtocol {
    var baseURL: String { get }
}

public enum Environment: EnvironmentProtocol {
    case development
    case staging
    case production

    public var baseURL: String {
        switch self {
        case .development:
            return "http://localhost:3100/dev/api/"
        case .staging:
            return "http://localhost:3100/staging/api"
        case .production:
            return "http://localhost:3100/api"
        }
    }
}
