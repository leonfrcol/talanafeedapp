//
//  URLRequestBuilder.swift
//  FeedApp
//
//  Created by DIF on 30/6/22.
//
import UIKit

public class URLRequestBuilder {
    @Injected(\.configurationManager) var configuration: ConfigurationProtocol

    var endpoint: String?
    var method: HTTPMethod = .get
    var headers: [String: Any]?
    var parameters: [String: Any]?

    @discardableResult
    public func withMethod(method: HTTPMethod) -> Self {
        self.method = method
        return self
    }

    @discardableResult
    public func withEndpoint(endpoint: String) -> Self {
        self.endpoint = endpoint
        return self
    }

    @discardableResult
    public func withHeaders(headers: [String: Any]?) -> Self {
        self.headers = headers
        return self
    }

    @discardableResult
    public func withParameters(parameters: [String: Any]?) -> Self {
        self.parameters = parameters
        return self
    }

    @discardableResult
    public func withJsonObject(jsonObject: Codable) -> Self {
        guard let params = jsonObject.dictionary else {
            return self
        }
        self.parameters = params
        return self
    }

    public func build() throws -> URLRequest? {
        do {
            guard let url = URL(string: configuration.environment.baseURL) else {
                return nil
            }
            var urlRequest = URLRequest(
                url: url.appendingPathComponent(endpoint ?? ""),
                cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                timeoutInterval: 100
            )
            urlRequest.httpMethod = method.rawValue
            headers?.forEach {
                guard let value = $0.value as? String else {
                    return
                }
                urlRequest.addValue(value, forHTTPHeaderField: $0.key)
            }

            if let params = parameters {
                urlRequest = try buildRequestParams(urlRequest, params: params)
            }
            print("urlRequest \(urlRequest)")
            return urlRequest
        } catch {
            print("Error")
            return nil
        }
    }

    func buildRequestParams(_ urlRequest: URLRequest, params: [String: Any]) throws -> URLRequest {
        var urlRequest = urlRequest
        guard let params = parameters else {
            return urlRequest
        }
        urlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return urlRequest
        }
        urlRequest.httpBody = httpBody
        return urlRequest
    }
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else {
        return nil
    }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
