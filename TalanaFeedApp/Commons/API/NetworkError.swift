//
//  NetworkError.swift
//  FeedApp
//
//  Created by DIF on 30/6/22.
//

public enum NetworkErrorStatusCode: Error, CustomStringConvertible {
    case notFound
    case unknown

    public var statusCode: Int {
        switch self {
        case .notFound:
            return 404
        case .unknown:
            return -1
        }
    }

    public var description: String {
        switch self {
        case .notFound:
            return "Not Found"
        case .unknown:
            return "Unknown error"
        }
    }
}

public final class NetworkError: Codable, Error {
    // MARK: - Properties
    public var statusCode: Int
    public var message: String
    public var domain: String?
    public var userInfo: String?

    // MARK: - Init
    public init(statusCode: Int, message: String, domain: String? = nil, userInfo: String? = nil) {
        self.statusCode = statusCode
        self.message = message
        self.domain = domain
        self.userInfo = userInfo
    }
}

extension NetworkError: CustomStringConvertible {
    public var description: String {
        var errorDescription = "Error code: \(statusCode), error.localizedDescription: \(message),"

        if let domain = domain {
            errorDescription += " error domain: \(domain);"
        }

        if let userInfo = userInfo {
            errorDescription += " error userinfo: \(userInfo);"
        }

        return errorDescription
    }
}
