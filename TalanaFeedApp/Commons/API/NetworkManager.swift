//
//  NetworkManager.swift
//  FeedApp
//
//  Created by DIF on 30/6/22.
//
import UIKit

public enum HTTPMethod: String {
    case get
    case post
    case put
}

public protocol NetworkManagerProtocol {
    func execute(with request: URLRequest?, completion: @escaping (Result<Data, NetworkError>) -> Void)
}

public class NetworkManager: NSObject, NetworkManagerProtocol {
    public typealias DataResult = Result<Data, NetworkError>

    public func execute(with request: URLRequest?, completion: @escaping (DataResult) -> Void) {
        guard let customRequest = request else {
            return
        }
        let urlSessionConfiguration = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: urlSessionConfiguration, delegate: self, delegateQueue: nil)

        let dataTask = urlSession.dataTask(with: customRequest, completionHandler: { data, response, error in
            if let error = error {
                let nsError = error as NSError
                let networkError = NetworkError(
                    statusCode: nsError.code,
                    message: nsError.localizedDescription,
                    domain: nsError.domain,
                    userInfo: nsError.userInfo.description
                )
                completion(.failure(networkError))
                return
            }

            guard let urlResponse = response as? HTTPURLResponse else {
                let networkError = NetworkError(
                    statusCode: NetworkErrorStatusCode.unknown.statusCode,
                    message: NetworkErrorStatusCode.unknown.description
                )
                completion(.failure(networkError))
                return
            }

            guard let data = data else {
                let networkError = NetworkError(
                    statusCode: NetworkErrorStatusCode.notFound.statusCode,
                    message: NetworkErrorStatusCode.notFound.description
                )
                completion(.failure(networkError))
                return
            }

            switch urlResponse.statusCode {
            case 200...299:
                completion(.success(data))
                return

            default:
                let networkError = NetworkError(
                    statusCode: urlResponse.statusCode,
                    message: urlResponse.debugDescription,
                    domain: urlResponse.debugDescription,
                    userInfo: urlResponse.allHeaderFields.description
            )
                completion(.failure(networkError))
                return
            }
        })
        dataTask.resume()
    }
}

extension NetworkManager: URLSessionDelegate {
    public func urlSession(
        _ session: URLSession,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?
        ) -> Void
    ) {
    }
}
