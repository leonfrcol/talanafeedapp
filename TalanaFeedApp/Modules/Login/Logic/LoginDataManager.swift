//
//  LoginDataManager.swift
//  TalanaFeedApp
//
//  Created by DIF on 3/7/22.
//

import UIKit

public protocol LoginDataManagerProtocol {
    func getLogin(request: LoginRequest, completion: @escaping (Result<LoginResponse, NetworkError>) -> Void)
}

public class LoginDataManager: LoginDataManagerProtocol {
    // MARK: - Properties
    @Injected(\.networkManager) var networkManager: NetworkManagerProtocol

    public init() {}

    // MARK: - LoginDataManagerProtocol
    public func getLogin(request: LoginRequest, completion: @escaping (Result<LoginResponse, NetworkError>) -> Void) {
        let request = try? URLRequestBuilder()
            .withMethod(method: .post)
            .withEndpoint(endpoint: "/login")
            .withJsonObject(jsonObject: request)
            .build()

        networkManager.execute(with: request) { result in
            switch result {
            case .success(let encodedResponse):
                do {
                    let dataDecode = try JSONDecoder().decode(LoginResponse.self, from: encodedResponse)
                    completion(.success(dataDecode))
                } catch let error as NSError {
                    let err: NSError = error as NSError
                    let error = NetworkError(statusCode: err.code, message: err.localizedDescription)
                    completion(.failure(error))
                }

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
