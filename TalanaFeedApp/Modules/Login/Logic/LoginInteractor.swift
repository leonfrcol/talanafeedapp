//
//  LoginInteractor.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import Foundation

protocol LoginPresenterToInteractorProtocol {
    var presenter: LoginInteractorToPresenterProtocol? { get set }

    func login(request: LoginRequest)
}

class LoginInteractor {
  // MARK: - Properties
  var presenter: LoginInteractorToPresenterProtocol?
  @Injected(\.loginDataManager) var loginDataManager: LoginDataManagerProtocol
}

// MARK: - LoginInteractorProtocol
extension LoginInteractor: LoginPresenterToInteractorProtocol {
    public func login(request: LoginRequest) {
        loginDataManager.getLogin(request: request, completion: { [weak self] resp in
            switch resp {
            case .success(let loginResponse):
                self?.presenter?.logginSuccess(response: loginResponse)
            case .failure(let error):
                self?.presenter?.logginError(error: error.description)
            }
        })
    }
}
