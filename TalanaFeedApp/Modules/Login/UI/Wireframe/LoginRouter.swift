//
//  LoginRouter.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//  Template generated by Fredy Leon
//

import Foundation
import UIKit

protocol LoginPresenterToRouterProtocol {
    static func createModule() -> LoginViewController
    func presentHome(navController: UINavigationController?)
}

public class LoginRouter: LoginPresenterToRouterProtocol {
    // MARK: - Methods
    static func createModule() -> LoginViewController {
        let view = LoginViewController()
        let presenter: LoginViewToPresenterProtocol & LoginInteractorToPresenterProtocol = LoginPresenter()

        view.presenter = presenter
        view.presenter?.router = LoginRouter()
        view.presenter?.view = view
        view.presenter?.interactor = LoginInteractor()
        view.presenter?.interactor?.presenter = presenter

        return view
    }

    public func presentHome(navController: UINavigationController?) {
        let detailMovieViewController: HomeViewController = HomeRouter.createModule()
        navController?.pushViewController(detailMovieViewController, animated: true)
    }
}
