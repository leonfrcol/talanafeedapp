//
//  LoginPresenter.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import UIKit

protocol LoginViewToPresenterProtocol {
    var view: LoginPresenterToViewProtocol? { get set }
    var interactor: LoginPresenterToInteractorProtocol? { get set }
    var router: LoginPresenterToRouterProtocol? { get set }

    func login(user: String?, password: String?)
    func goToHome(navController: UINavigationController?)
}

protocol LoginInteractorToPresenterProtocol {
    func logginSuccess(response: LoginResponse)
    func logginError(error: String)
}

class LoginPresenter {
  // MARK: - Properties
  var view: LoginPresenterToViewProtocol?
  var interactor: LoginPresenterToInteractorProtocol?
  var router: LoginPresenterToRouterProtocol?
}

// MARK: - LoginPresenterProtocol
extension LoginPresenter: LoginViewToPresenterProtocol {
    public func login(user: String?, password: String?) {
        guard let usr = user else {
            return
        }

        guard let psdw = password else {
            return
        }

        let loginRequest = LoginRequest(username: usr, password: psdw)
        interactor?.login(request: loginRequest)
    }

    public func goToHome(navController: UINavigationController?) {
        router?.presentHome(navController: navController)
    }
}

extension LoginPresenter: LoginInteractorToPresenterProtocol {
    public func logginSuccess(response: LoginResponse) {
        view?.loginSuccess()
    }

    public func logginError(error: String) {
        view?.loginError(error: error)
    }
}
