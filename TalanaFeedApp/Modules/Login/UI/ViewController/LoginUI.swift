//
//  LoginUI.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import Foundation
import UIKit

class LoginUI: NSObject {
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblUserName: UILabel!
    @IBOutlet private weak var tfUserName: UITextField!
    @IBOutlet private weak var lblPassword: UILabel!
    @IBOutlet private weak var tfPassword: UITextField!
    @IBOutlet private weak var btnContinue: UIButton!

    func load(viewController: UIViewController) {
        setupLabelStyle()
        setupButton()

        setupTextField(textField: tfUserName)
        setupTextField(textField: tfPassword)

        setupBlur(view: viewController.view)
    }

    private func setupLabelStyle() {
        lblTitle.text = "Feeds App"
        lblTitle.font = UIFont.italicSystemFont(ofSize: 30.0)
        lblTitle.textAlignment = .center
        lblTitle.textColor = .orange

        lblUserName.text = "Username"
        lblUserName.font = UIFont.boldSystemFont(ofSize: 20.0)

        lblPassword.text = "Password"
        lblPassword.font = UIFont.boldSystemFont(ofSize: 20.0)
    }

    private func setupButton() {
        btnContinue.setTitle("Continuar", for: .normal)
        btnContinue.tintColor = .white
        btnContinue.backgroundColor = .orange
        btnContinue.layer.cornerRadius = 10
        btnContinue.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
        btnContinue.addConstraint(btnContinue.heightAnchor.constraint(equalToConstant: 44))
    }

    private func setupTextField(textField: UITextField) {
        textField.layer.cornerRadius = 10
        textField.layer.borderColor = UIColor.orange.cgColor
        textField.backgroundColor = .lightGray
        textField.layer.borderWidth = 1.0
        textField.layer.masksToBounds = true
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 0))
        textField.leftViewMode = UITextField.ViewMode.always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 0))
        textField.rightViewMode = UITextField.ViewMode.always
        textField.addConstraint(textField.heightAnchor.constraint(equalToConstant: 44))
    }

    private func setupBlur(view: UIView) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.bringSubviewToFront(lblTitle)
        view.bringSubviewToFront(lblUserName)
        view.bringSubviewToFront(tfUserName)
        view.bringSubviewToFront(lblPassword)
        view.bringSubviewToFront(tfPassword)
        view.bringSubviewToFront(btnContinue)
    }
}
