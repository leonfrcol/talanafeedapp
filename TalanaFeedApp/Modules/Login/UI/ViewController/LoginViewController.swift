//
//  LoginViewController.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import UIKit

protocol LoginPresenterToViewProtocol {
    func loginSuccess()
    func loginError(error: String)
}

class LoginViewController: UIViewController {
  // MARK: - Properties
  var presenter: LoginViewToPresenterProtocol?
    @IBOutlet private weak var loginUI: LoginUI!
    @IBOutlet private weak var username: UITextField!
    @IBOutlet private weak var password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginUI.load(viewController: self)
    }

    @IBAction private func btnContinuePressed(_ sender: Any) {
        presenter?.login(user: username?.text, password: password?.text)
    }
}

// MARK: - LoginViewProtocol
extension LoginViewController: LoginPresenterToViewProtocol {
    public func loginSuccess() {
        DispatchQueue.main.async {
            self.presenter?.goToHome(navController: self.navigationController)
        }
    }

    func loginError(error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Error",
                message: "User or Password invalid",
                preferredStyle: UIAlertController.Style.alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
