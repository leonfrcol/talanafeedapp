//
//  LoginRequest.swift
//  Project: FeedApp
//
//  Module: Login
//
//  By DIF 29/6/22
//

public struct LoginRequest: Codable {
    var username: String
    var password: String
}
