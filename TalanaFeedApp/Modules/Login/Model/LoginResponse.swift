//
//  LoginResponse.swift
//  FeedApp
//
//  Created by DIF on 30/6/22.
//

public struct LoginResponse: Codable {
    var status: String
    var token: String

    enum CodingKeys: String, CodingKey {
        case status = "STATUS"
        case token = "api-token"
    }
}
