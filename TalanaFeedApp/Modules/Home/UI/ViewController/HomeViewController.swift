//
//  HomeViewController.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import UIKit
import IGListKit
import CoreData

protocol HomePresenterToViewProtocol {
    func feedSuccess(listFeed: [FeedModel])
    func feedError()
}

class HomeViewController: UIViewController {
    // MARK: - Properties
    var presenter: HomeViewToPresenterProtocol?
    var listFeed: [FeedModel] = []
    private var adapter: ListAdapter?

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var homeUI: HomeUI!

    override func viewDidLoad() {
        super.viewDidLoad()
        homeUI.load(viewController: self)
        presenter?.goFeeds()
        setup()
    }

    private func setup() {
        adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter?.collectionView = collectionView
        adapter?.dataSource = self
    }

    @IBAction private func btnGoDetail(_ sender: Any) {
        presenter?.goToDetail(navController: self.navigationController)
    }
}

// MARK: - HomeViewProtocol
extension HomeViewController: HomePresenterToViewProtocol {
    func feedSuccess(listFeed: [FeedModel]) {
        self.listFeed = listFeed
        DispatchQueue.main.async {
            self.adapter?.performUpdates(animated: true, completion: nil)
        }
    }

    func feedError() {
    }
}

extension HomeViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        self.listFeed
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        FeedSectionController()
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        nil
    }
}

extension HomeViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }

    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange sectionInfo: NSFetchedResultsSectionInfo,
        atSectionIndex sectionIndex: Int,
        for type: NSFetchedResultsChangeType
    ) {
    }

    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?
    ) {
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }
}
