//
//  HomeUI.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import Foundation
import UIKit

class HomeUI: NSObject {
    @IBOutlet private weak var collectionView: UICollectionView!

    func load(viewController: UIViewController) {
        viewController.navigationItem.setHidesBackButton(true, animated: false)
        viewController.title = "Feeds List"
    }
}
