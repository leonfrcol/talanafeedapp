//
//  HomePresenter.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import UIKit

protocol HomeViewToPresenterProtocol {
    var view: HomePresenterToViewProtocol? { get set }
    var interactor: HomePresenterToInteractorProtocol? { get set }
    var router: HomePresenterToRouterProtocol? { get set }
    var feedItemSelected: FeedModel? { get set }

    func goFeeds()
    func goToDetail(navController: UINavigationController?)
}

protocol HomeInteractorToPresenterProtocol {
    func feedSuccess(listFeed: [FeedModel])
    func feedError(error: String)
}

class HomePresenter {
  // MARK: - Properties
  var view: HomePresenterToViewProtocol?
  var interactor: HomePresenterToInteractorProtocol?
  var router: HomePresenterToRouterProtocol?
  var feedItemSelected: FeedModel?
}

// MARK: - HomePresenterProtocol
extension HomePresenter: HomeViewToPresenterProtocol {
    func goFeeds() {
        interactor?.fetchFeeds()
    }

    public func goToDetail(navController: UINavigationController?) {
        guard let feedSelected = feedItemSelected else {
            return
        }
        router?.presentDetail(feed: feedSelected, navController: navController)
    }
}

extension HomePresenter: HomeInteractorToPresenterProtocol {
    func feedSuccess(listFeed: [FeedModel]) {
        view?.feedSuccess(listFeed: listFeed)
    }

    func feedError(error: String) {
    }
}
