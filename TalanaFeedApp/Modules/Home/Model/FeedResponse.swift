//
//  FeedResponse.swift
//  TalanaFeedApp
//
//  Created by DIF on 3/7/22.
//

import Foundation
import IGListKit

// MARK: - FeedResponseElement
public struct Feed: Codable {
    let id: Int?
    let title: String?
    let image: String?
    let date: String?
    let feedResponseDescription: String?
    let published: String?
    let authorID: String?
    let link: String?

    enum CodingKeys: String, CodingKey {
        case id, title, image, date
        case feedResponseDescription = "description"
        case published
        case authorID = "author_id"
        case link
    }
}

public typealias FeedResponse = [Feed]
