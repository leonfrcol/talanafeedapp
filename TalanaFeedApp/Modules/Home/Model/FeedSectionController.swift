//
//  FeedSectionController.swift
//  TalanaFeedApp
//
//  Created by DIF on 4/7/22.
//

import IGListKit

class FeedSectionController: ListSectionController {
    var currentFeed: FeedModel?
    var presenter: HomeViewToPresenterProtocol?

    override init() {
        super.init()
        inset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }

    override func didUpdate(to object: Any) {
        guard let feed = object as? FeedModel else {
            return
        }
        currentFeed = feed
    }

    override func numberOfItems() -> Int {
        1
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(
            of: FeedCell.self, for: self, at: index
        ) as? FeedCell else {
            return UICollectionViewCell()
        }

        cell.configureCell(feedModel: currentFeed)
        return cell
    }

    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        return CGSize(width: width, height: 100)
    }

    override func didSelectItem(at index: Int) {
        guard let vc = viewController as? HomeViewController else {
            return
        }
        vc.presenter?.feedItemSelected = currentFeed
        vc.presenter?.goToDetail(navController: viewController?.navigationController)
    }
}
