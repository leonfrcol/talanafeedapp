//
//  FeedCell.swift
//  TalanaFeedApp
//
//  Created by DIF on 4/7/22.
//

import UIKit
import Nuke

public final class FeedCell: UICollectionViewCell {
    let title: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.italicSystemFont(ofSize: 14.0)
        label.numberOfLines = 0
        return label
    }()

    var customProcessors: [ImageProcessing] = {
        var imgProcessors = [ImageProcessing]()
        return imgProcessors
    }()

    let imageView: UIImageView = {
        let imageV = UIImageView()
        imageV.layer.masksToBounds = true
        return imageV
    }()

    let published: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    let widthImage: CGFloat = {
        80
    }()

    let heightImage: CGFloat = {
        80
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 16

        contentView.addSubviewForAutoLayout(imageView)
        imageView.anchor(left: contentView.leftAnchor, paddingLeft: 10, width: widthImage, height: heightImage)
        imageView.centerY(inView: contentView)

        contentView.addSubviewForAutoLayout(title)
        title.anchor(
            top: contentView.topAnchor,
            left: imageView.rightAnchor,
            right: contentView.rightAnchor,
            paddingTop: 10,
            paddingLeft: 10
        )

        contentView.addSubviewForAutoLayout(published)
        published.anchor(
            left: imageView.rightAnchor,
            bottom: contentView.bottomAnchor,
            right: contentView.rightAnchor,
            paddingLeft: 10,
            paddingBottom: 5
        )

        setupImageView()
        setupColor()
    }

    private func setupImageView() {
        customProcessors.append(ImageProcessors.Resize(size: CGSize(width: widthImage, height: heightImage)))
        customProcessors.append(ImageProcessors.Circle())
    }

    private func setupColor() {
        backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
    }

    required init?(coder: NSCoder) {
        fatalError("Error fatal")
    }

    public func configureCell(feedModel: FeedModel?) {
        title.text = feedModel?.title.lowercased().capitalized ?? ""
        published.text = feedModel?.published.lowercased().capitalized ?? ""

        let optionsLoading = ImageLoadingOptions(
            placeholder: UIImage(named: "loader"), transition: .fadeIn(duration: 3.33)
        )
        guard let img = feedModel?.image, let urlImage = URL(string: img) else {
            return
        }
        loadImage(urlImage, view: imageView, processors: customProcessors, options: optionsLoading)
    }

    private func loadImage(
        _ url: URL,
        view: UIImageView,
        processors: [ImageProcessing],
        options: ImageLoadingOptions?
    ) {
        let request = ImageRequest(url: url, processors: processors)

        guard let loadingOptions = options else {
            Nuke.loadImage(with: request, into: view)
            return
        }

        Nuke.loadImage(with: request, options: loadingOptions, into: view)
    }
}
