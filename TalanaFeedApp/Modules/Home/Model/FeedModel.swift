//
//  FeedModel.swift
//  TalanaFeedApp
//
//  Created by DIF on 4/7/22.
//

import IGListKit

public class FeedModel {
    private var id: Int = 0
    private(set) var title: String
    private(set) var image: String
    private(set) var description: String
    private(set) var published: String
    private(set) var author_id: String
    private(set) var link: String

    init(
        id: Int,
        title: String,
        image: String,
        description: String,
        published: String,
        author_id: String,
        link: String
    ) {
            self.id = id
            self.title = title
            self.image = image
            self.description = description
            self.published = published
            self.author_id = author_id
            self.link = link
    }
}

extension FeedModel: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        String(id) as NSString
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? FeedModel else {
       return false
    }
    return self.id == object.id
  }
}
