//
//  HomeDataManager.swift
//  TalanaFeedApp
//
//  Created by DIF on 3/7/22.
//

import UIKit

public protocol HomeDataManagerProtocol {
    func getFeeds(completion: @escaping (Result<[FeedModel], NetworkError>) -> Void)
}

public class HomeDataManager: HomeDataManagerProtocol {
    // MARK: - Properties
    @Injected(\.networkManager) var networkManager: NetworkManagerProtocol

    public init() {}

    // MARK: - LoginDataManagerProtocol
    public func getFeeds(completion: @escaping (Result<[FeedModel], NetworkError>) -> Void) {
        let request = try? URLRequestBuilder()
            .withMethod(method: .get)
            .withEndpoint(endpoint: "/feed")
            .build()

        networkManager.execute(with: request) { result in
            switch result {
            case .success(let encodedResponse):
                do {
                    let feedResponseDecode = try JSONDecoder().decode(FeedResponse.self, from: encodedResponse)
                    let feedResponseModel = self.trasformIGListModel(listFeed: feedResponseDecode)
                    completion(.success(feedResponseModel))
                } catch let error as NSError {
                    let err: NSError = error as NSError
                    let error = NetworkError(statusCode: err.code, message: err.localizedDescription)
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    private func trasformIGListModel(listFeed: FeedResponse) -> [FeedModel] {
        var listFeedModel = [FeedModel]()
        for item in listFeed {
            let item = FeedModel(
                id: item.id ?? 0,
                title: item.title ?? "",
                image: item.image ?? "",
                description: item.feedResponseDescription ?? "",
                published: item.published ?? "",
                author_id: item.authorID ?? "",
                link: item.link ?? ""
            )
            listFeedModel.append(item)
        }
        return listFeedModel
    }
}
