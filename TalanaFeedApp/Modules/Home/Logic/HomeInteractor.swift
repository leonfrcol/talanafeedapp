//
//  HomeInteractor.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import Foundation

protocol HomePresenterToInteractorProtocol {
    var presenter: HomeInteractorToPresenterProtocol? { get set }

    func fetchFeeds()
}

class HomeInteractor {
  // MARK: - Properties
  var presenter: HomeInteractorToPresenterProtocol?
  @Injected(\.homeDataManager) var homeDataManager: HomeDataManagerProtocol
}

// MARK: - HomeInteractorProtocol
extension HomeInteractor: HomePresenterToInteractorProtocol {
    public func fetchFeeds() {
        homeDataManager.getFeeds(completion: { [weak self] resp in
            switch resp {
            case .success(let feedModelResponse):
                self?.presenter?.feedSuccess(listFeed: feedModelResponse)
            case .failure(let error):
                self?.presenter?.feedError(error: error.description)
            }
        })
    }
}
