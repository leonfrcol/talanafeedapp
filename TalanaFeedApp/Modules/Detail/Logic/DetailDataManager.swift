//
//  DetailDataManager.swift
//  TalanaFeedApp
//
//  Created by DIF on 3/7/22.
//

import Foundation
import UIKit
import CoreData

public protocol DetailDataManagerProtocol {
    var feedSelected: FeedModel? { get set }

    func saveFavorite()
}

public class DetailDataManager: DetailDataManagerProtocol {
    // MARK: - Properties
    public var feedSelected: FeedModel?

    var moc: NSManagedObjectContext!
    let appDelegate = UIApplication.shared.delegate as? AppDelegate

    public init() {
        moc = appDelegate?.persistentContainer.viewContext
    }

    public func saveFavorite() {
        let feed = FavoritesFeed(context: moc)
        feed.title = feedSelected?.title
        feed.image = feedSelected?.image
        feed.published = feedSelected?.published
        feed.author_id = feedSelected?.author_id
        appDelegate?.saveContext()
    }
}
