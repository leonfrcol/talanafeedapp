//
//  DetailInteractor.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import Foundation

protocol DetailPresenterToInteractorProtocol {
    func setFeedElement(feed: FeedModel)
    func getFeedElement() -> FeedModel?
    func saveFavorite()
}

class DetailInteractor {
  // MARK: - Properties
  @Injected(\.detailDataManager) var detailDataManager: DetailDataManagerProtocol
}

// MARK: - DetailInteractorProtocol
extension DetailInteractor: DetailPresenterToInteractorProtocol {
    func setFeedElement(feed: FeedModel) {
        detailDataManager.feedSelected = feed
    }

    func getFeedElement() -> FeedModel? {
        detailDataManager.feedSelected
    }

    func saveFavorite() {
        detailDataManager.saveFavorite()
    }
}
