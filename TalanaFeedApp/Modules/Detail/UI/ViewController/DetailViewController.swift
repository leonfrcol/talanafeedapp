//
//  DetailViewController.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import UIKit
import Nuke

protocol DetailViewProtocol {
    var feedSelected: FeedModel? { get set }

    func showFeedSelected()
}

class DetailViewController: UIViewController {
  // MARK: - Properties
  var presenter: DetailViewToPresenterProtocol?
    var feedSelected: FeedModel?

    var customProcessors: [ImageProcessing] = {
        var imgProcessors = [ImageProcessing]()
        return imgProcessors
    }()

    @IBOutlet private weak var imgView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptioTxtv: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.numberOfLines = 0
        showFeedSelected()
    }

    @IBAction private func btnAddFavorite(_ sender: Any) {
        presenter?.setFavorite()
    }
}

// MARK: - DetailViewProtocol
extension DetailViewController: DetailViewProtocol {
    func showFeedSelected() {
        feedSelected = presenter?.getFeedSeledted()

        titleLabel.text = feedSelected?.title
        descriptioTxtv.text = feedSelected?.description

        customProcessors.append(ImageProcessors.Resize(size: CGSize(width: 400, height: 200)))
        customProcessors.append(ImageProcessors.RoundedCorners(radius: 16))

        let optionsLoading = ImageLoadingOptions(
            placeholder: UIImage(named: "loader"),
            transition: .fadeIn(duration: 0.33)
        )
        guard let img = feedSelected?.image, let urlImage = URL(string: img) else {
            return
        }
        loadImage(urlImage, view: imgView, processors: customProcessors, options: optionsLoading)
    }

    private func loadImage(
        _ url: URL,
        view: UIImageView,
        processors: [ImageProcessing],
        options: ImageLoadingOptions?
    ) {
        let request = ImageRequest(url: url, processors: processors)

        guard let loadingOptions = options else {
            Nuke.loadImage(with: request, into: view)
            return
        }

        Nuke.loadImage(with: request, options: loadingOptions, into: view)
    }
}
