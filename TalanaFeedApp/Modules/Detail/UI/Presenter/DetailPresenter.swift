//
//  DetailPresenter.swift
//  TalanaFeedApp
//
//  Created DIF on 1/7/22.
//

import UIKit

protocol DetailViewToPresenterProtocol {
    var view: DetailViewProtocol? { get set }
    var interactor: DetailPresenterToInteractorProtocol? { get set }
    var router: DetailPresenterToRouterProtocol? { get set }

    func getFeedSeledted() -> FeedModel?
    func setFavorite()
}

class DetailPresenter {
  // MARK: - Properties
  var view: DetailViewProtocol?
  var interactor: DetailPresenterToInteractorProtocol?
  var router: DetailPresenterToRouterProtocol?
}

// MARK: - DetailPresenterProtocol
extension DetailPresenter: DetailViewToPresenterProtocol {
    func getFeedSeledted() -> FeedModel? {
        interactor?.getFeedElement()
    }

    func setFavorite() {
        interactor?.saveFavorite()
    }
}
